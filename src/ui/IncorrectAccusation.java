package ui;

import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
/**
 * @author hflin
 */
public class IncorrectAccusation extends JFrame {
	
	JLabel ia;
	
	
	public IncorrectAccusation(){
		super("Incorrect Accusation");
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		GridLayout grid = new GridLayout(2, 1);
		setLayout(grid);
		
		ia = new JLabel("incorrect accusation");
		
		add(ia);
		setLocationRelativeTo(null);
		setVisible(true);
		pack();
	}
	
	public Dimension getPreferredSize(){
		return new Dimension(512, 256);
		
	}

}
