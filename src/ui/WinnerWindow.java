package ui;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.*;

/**
* The window that is shown when a player has entered a correct accusation and won the game.
*
* @author  declanho
*/
public class WinnerWindow extends JFrame {
	JLabel _pictureLabel;
	JButton _okay;
	
	public WinnerWindow()
	{
		super("YOU ARE WINNER");
		
	    setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	    
	    FlowLayout gridLayout = new FlowLayout();
	    setLayout(gridLayout);
	    
	    BufferedImage image = null;
		try {
			image = ImageIO.read(getClass().getResourceAsStream("/winner.png"));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
	    _pictureLabel = new JLabel(new ImageIcon(image));
	    add(_pictureLabel);
	    
	    _okay = new JButton("THANKS");
	    _okay.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e)
			{
				// Close the window
				dispose();
			}
		});
	    
	    add(_okay);
	    
	    pack();
	    
	    setLocationRelativeTo(null);
	    setVisible(true);
	    
	}
}
