package ui;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.*;

import model.Card;
import model.Game;
import model.Player;

/**
* The window that is responsible for handling the player making an accusation.
*
* @author  declanho
* @author jtsutten
* @author hflin
* @author artuoto
*/
public class AccusationWindow extends JFrame {
	JTextField _suspect;
	JTextField _room;
	JTextField _weapon;
	JButton _submit;
	Game _game;
	ArrayList<Card> _envelope;
	
	String[] _weaponCards = {"Candlestick",
    		"Poison",
    		"Rope",
    		"Gloves",
    		"Horseshoe",
    		"Knife",
    		"Lead Pipe",
    		"Revolver",
    		"Wrench"};
    String[] _roomCards = {"Study",
    		"Hall",
    		"Lounge",
    		"Dining Room",
    		"Kitchen",
    		"Ballroom",
    		"Conservatory",
    		"Billiard Room",
    		"Library"};
    String[] _playerCards = {"Ms. Scarlet",
    		"Col. Mustard",
    		"Mrs. White",
    		"Rev. Green",
    		"Mrs. Peacock",
    		"Prof. Plum"};
    
	public AccusationWindow(Game game)
	{
		super("Make an Accusation");
		_game = game;
		_envelope = _game.getEnvelope();
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	    
	    GridLayout gridLayout = new GridLayout(4, 1, 0, 8);
	    setLayout(gridLayout);
	    
	    _suspect = new JTextField("Suspect");
	    _room = new JTextField("Room");
	    _weapon = new JTextField("Weapon");
	    _submit = new JButton("Submit");
	    //_submit.setEnabled(false);	    
	    _submit.addActionListener(new ActionListener() {
	    	
	    	@Override
			public void actionPerformed(ActionEvent e)
			{
				System.out.println("Accused " + _suspect.getText() + " in room " + _room.getText() + " with weapon " + _weapon.getText() + "!");
				boolean hasWeapon = _envelope.get(0).getName().toLowerCase().equals(_weapon.getText().toLowerCase());
				boolean hasRoom = _envelope.get(1).getName().toLowerCase().equals(_room.getText().toLowerCase());
				boolean hasSuspect = _envelope.get(2).getName().toLowerCase().equals(_suspect.getText().toLowerCase());
				// the if statement
				boolean doTheSubmittedSatisfy = false;
				doTheSubmittedSatisfy = checkToEnableSubmit();
				
				
				if(doTheSubmittedSatisfy == true){
					
				
				if (hasWeapon && hasRoom && hasSuspect) {
					new WinnerWindow();
				}
				else {
					new IncorrectAccusation();
					int currentTurn = game.getCurrentTurn();
					Player currentPlayer = game.getCurrentPlayer().get(currentTurn);
					currentPlayer.setDead(true);
				}
				dispose();
				}else if(doTheSubmittedSatisfy == false){
					
				}
			}
		});
	    
	    add(_suspect);
	    add(_room);
	    add(_weapon);
	    add(_submit);
	    
	    pack();
	    
	    setLocationRelativeTo(null);
	    setVisible(true);
	}
	
	/**
	 * checks to see if the entries are valid to enable the submit button
	 * @return true if the entries are valid
	 */
	public boolean checkToEnableSubmit() {
		boolean weaponCardSubmitted = false;
    	boolean roomCardSubmitted = false;
    	boolean playerCardSubmitted = false;
    	boolean allCardsSubmitted = false;
 
			for (int i=0; i< _playerCards.length; i++) {
				if (_suspect.getText().toLowerCase().equals(_playerCards[i].toLowerCase())) {
					playerCardSubmitted = true;
				}
			}
			for (int i=0; i< _roomCards.length; i++) {
				if (_room.getText().toLowerCase().equals(_roomCards[i].toLowerCase())) {
					roomCardSubmitted = true;
				}
			}
				
			for (int i=0; i< _weaponCards.length; i++) {
				if (_weapon.getText().toLowerCase().equals(_weaponCards[i].toLowerCase())) {
					weaponCardSubmitted = true;
				}
			}
			allCardsSubmitted = playerCardSubmitted && roomCardSubmitted && weaponCardSubmitted;
		
		if (allCardsSubmitted) {
			return true;
		}
		return false;
	}
	   @Override
	   public Dimension getPreferredSize() {
	      return new Dimension(512, 256);
	   }
}
